//import logo from './logo.svg';
import {useState,useEffect} from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';

import { UserProvider } from './UserContext';

import './App.css';

import AppNavbar from './components/AppNavbar';
import AdminDashboard from './pages/AdminDashboard';
import Cart from './pages/Cart';
 
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout'
import Error from './pages/Error';



function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }


  useEffect(() => {
    

    

    if(localStorage.getItem('token')){
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        method:"POST",
        headers: {
          Authorization : `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(res => res.json())
      .catch(err=> {
        console.log("ERROR in fetching details");
      })
      .then(data => {
        //console.log(data._id)

        // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
        //console.log(typeof data._id)
        if(data._id){
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        }
      })
    }

    //console.log(user.id)
    //console.log(user)
    //console.log(user.isAdmin)

  },[])


  return (
    
    <UserProvider value={{user,setUser,unsetUser}}>
      <Router>
        <Container fluid>
            <AppNavbar />
            <Container>
              <Routes>
                  <Route path="/" element={<Home />} />
                  <Route path="/products" element={<Products />} />
                  <Route path="/admin" element={<AdminDashboard />}/>
                  <Route path="/cart" element={<Cart />}/>
                  <Route path="/products/:productId" element={<ProductView />} />
                  <Route path="/register" element={<Register />} />
                  <Route path="/login" element={<Login />} />
                  <Route path="/logout" element={<Logout />} />
                  <Route path="*" element={<Error />} />
              </Routes>
            </Container>          
        </Container>
      </Router>
    </UserProvider>
    
  );
}

export default App;
