import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function AddProduct({unShowAddProduct,change}){

	const { user,setUser} = useContext(UserContext);

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [password, setPassword] = useState('');

	function saveProduct(e) {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products`,{
			method: "POST",
			headers: {
				"Content-Type": 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},

			
			body: JSON.stringify({
				name : name,
				description: description,
				price: price
			})

		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
	    			  icon: 'success',
	    			  title: 'Add Product Successfull',
	    			  text: 'yey'
	    			  
	    			})

				unShowAddProduct();
				change();

			} else {
				Swal.fire({
	    			  icon: 'error',
	    			  title: 'Add Product failed',
	    			  text: 'Somethin went wrong'
	    			  
	    			})
			}
		})
	}

	return(

		<Form onSubmit={(e) => saveProduct(e)}>
			{/*//<h2>Login</h2>*/}
			<Form.Group controlId="productName" className="mb-2">
				{/*<Form.Label>P Address</Form.Label>*/}
				<Form.Control
					type="text"
					placeholder="Enter a product name"
					value={ name }
					onChange={e => setName(e.target.value)}
					required
				 />
				 {/*<Form.Text className="text-muted">We'll never share your email with anyone.</Form.Text>*/}
			</Form.Group>

			<Form.Group controlId="productDescription" className="mb-2">
				{/*<Form.Label>P Address</Form.Label>*/}
				<Form.Control
					type="text"
					placeholder="Enter a product description"
					value={ description }
					onChange={e => setDescription(e.target.value)}
					required
				 />
				 {/*<Form.Text className="text-muted">We'll never share your email with anyone.</Form.Text>*/}
			</Form.Group>

			<Form.Group controlId="productPrice" className="mb-2">
				{/*<Form.Label>P Address</Form.Label>*/}
				<Form.Control
					type="number"
					placeholder="Enter a product price"
					value={ price }
					onChange={e => setPrice(e.target.value)}
					required
				 />
				 {/*<Form.Text className="text-muted">We'll never share your email with anyone.</Form.Text>*/}
			</Form.Group>

			<Button variant="primary" type="submit" id="submitBtn" className="mt-2">Save Product</Button>
			<Button variant="secondary" onClick={unShowAddProduct} className="mt-2">Cancel</Button>

			{/*<Form.Group controlId="password2">
				<Form.Label>Confirm Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Confirm your password"
					value={ password2 }
					onChange={e => setPassword2(e.target.value)}
					required
				 />
			</Form.Group>*/}
			
			
		</Form>

	)
}