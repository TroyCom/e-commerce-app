import {useState,useEffect,useContext} from 'react';
import {Card,Button,Row,Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

import UpdateProductForm from './UpdateProductForm';


export default function ProductCard({product,change,page}) {

	//console.log(product);
	//console.log(typeof product);

	const {_id,name,description,price,isActive} = product;
	const[isProductActive, setIsProductActive] = useState(isActive);
	const[showProductUpdateForm,setShowProductUpdateForm] = useState(false);

	const {user} = useContext(UserContext);

	function activateProduct(productId){

		//e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`,{
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}

		})
		.then(res => res.json())
		.then(data => {

			if(data){
				Swal.fire({
	    			  icon: 'success',
	    			  title: 'Activation Successfull',
	    			  text: 'yey'
	    			  
	    			})
				setIsProductActive(true);
			} else {
				Swal.fire({
	    			  icon: 'error',
	    			  title: 'Activation failed',
	    			  text: 'Somethin went wrong'
	    			  
	    			})
			}
		})
	}

	function deactivateProduct(productId){
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}

		})
		.then(res => res.json())
		.then(data => {

			if(data){
				Swal.fire({
	    			  icon: 'success',
	    			  title: 'Deactivation Successfull',
	    			  text: 'yey'
	    			  
	    			})
				setIsProductActive(false);
			} else {
				Swal.fire({
	    			  icon: 'error',
	    			  title: 'Deactivation failed',
	    			  text: 'Somethin went wrong'
	    			  
	    			})
			}
		})
	}


	


	//console.log(`yeahhhh${typeof _id}`)

	return (

		
				<Card className="productCard p-3 mt-2 bg-dark text-light">
					<Row>
						<Col>
							<Card.Body className="p-1">
								<Card.Title>
									<h2>{ name }</h2>
								</Card.Title>
								<Card.Subtitle>Description:</Card.Subtitle>
								<Card.Text>
									{description}	
								</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>PHP {price}</Card.Text>
								{/*{
								
									(user.isAdmin) ? 
										<Card.Subtitle>Active? : {String(isActive)}</Card.Subtitle>


										
										:
										null
								
								}

								{
									(isActive) ?
											<Button variant="danger">Deactivate</Button>
											:
											<Button variant="primary">Activate</Button>
								}*/}

								<Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>

							</Card.Body>
						</Col>

						{
							(user.isAdmin == null || user.isAdmin == false || page == "catalog") ?
								null
							:
								<Col>
									<div className = "p-3">
										<Card.Subtitle>Active? : {String(isProductActive)}</Card.Subtitle>
										{
											(isProductActive) ?
												<Button variant="danger" onClick={()=> deactivateProduct(_id)}>Deactivate</Button>
												:
												<Button variant="primary" onClick={() => activateProduct(_id)}>Activate</Button>
										}

										{
											(showProductUpdateForm == false) ?
												<Button variant="primary" onClick={()=>{
													setShowProductUpdateForm(true);
												}} >Update</Button>
												:
												<UpdateProductForm change = {() => change()}updatedProductId ={product._id} unShow ={() => setShowProductUpdateForm(false)} />
										}
										

										

									</div>
								</Col>
						}

						
					</Row>
					
				</Card>

	)
}