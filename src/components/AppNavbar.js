import {useContext} from 'react';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {Link, NavLink} from 'react-router-dom'

import UserContext from '../UserContext';

export default function AppNavbar() {

	//const [user,setUser] = useState(localStorage.getItem("email"));
	//console.log(user);
	const {user} = useContext(UserContext)
	console.log(user.id);

	return (

		<Navbar bg="light" expand="lg">
			<Container fluid>
				<Navbar.Brand as={NavLink} to="/">Troy Shop</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ms-auto">
						<Nav.Link as={NavLink} to="/">Home</Nav.Link>
						<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
						{
							(user.id !== null && user.isAdmin == false) ?
								<Nav.Link as={NavLink} to="/cart" >Cart</Nav.Link>
								:
								null
						}
						{
							(user.isAdmin == true) ?
								<Nav.Link as={NavLink} to="/admin" >DashBoard</Nav.Link>
								:
								null
						}
						{
							(user.id !== null && user.id !== undefined) ?
								<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>

								:
								<>
									<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
									<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
								</>
						}

					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>

	)
}
