import {Button,Col,Row} from 'react-bootstrap';
import {Link} from 'react-router-dom'

export default function Banner(props) {
	console.log(props.page)
	return (
		(props.page == "home") ?
			<Row>
				<Col className = "p-5">
					<h1>Troy Shop</h1>
					<p>Amazing Prices and The Best Deals</p>
					<Link className="btn btn-primary btn-block" to="/products">Shop Now</Link>
				</Col>
			</Row>

			:

			<Row>
				<Col className = "p-5">
					<h1>Page Not Found</h1>
					<p>Go back to <Link to="/">homepage</Link></p>
				
					
				</Col>
			</Row>

	)
}