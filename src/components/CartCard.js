import {useState,useEffect,useContext} from 'react';
import {Form,Card,Button,Row,Col,InputGroup} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function CartCard({product,quantity}){


	const {_id,name,description,price,isActive} = product;

	const [quantityHere,setQuantityHere] = useState(quantity);

	const [subTotal,setSubTotal] = useState(0);

	//setSubTotal(price * quantityHere);

	let subTotal1 = price*quantityHere;

	return(

		<Card className='cartCard p-2 bg-dark text-light mt-2'>
			<Row>

				
					<Col>
						<Card.Title>{name}</Card.Title>
					</Col>
					<Col>
						<Card.Title>{description}</Card.Title>
					</Col>
					<Col>
						<Card.Title>{price}</Card.Title>
					</Col>

					<Col>
						<InputGroup >
							{/*<InputGroup.Text id="basic-addon1">quantity</InputGroup.Text>*/}
							<Form.Control
								type="number"
								value={ quantityHere }
								min = '1'
								onChange={e => setQuantityHere(e.target.value)}

								required

							/>
									
						</InputGroup>
					</Col>
					<Col>
						<Card.Title>subTotal: {subTotal1}</Card.Title>
					</Col>

				
			</Row>
		</Card>
	)
}