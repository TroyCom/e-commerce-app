import { useState,useEffect,useContext } from 'react';
import { Container, Card, Button,Form, Row, Col,InputGroup} from 'react-bootstrap';
import {useParams, Link ,useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView(){

		const {productId} = useParams();

		const navigate = useNavigate();

		const {user} = useContext(UserContext);
		console.log(productId);
		const [name, setName] = useState("");
		const [description, setDescription] = useState("");
		const [price, setPrice] = useState(0);
		const [quantity,setQuantity] = useState(1);

		const checkOutProducts = () => {
			fetch(`${process.env.REACT_APP_API_URL}/users/checkOut`,{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					Authorization : `Bearer ${localStorage.getItem('token')}`
				},
				body : JSON.stringify([{
						productId : productId,
						quantity: quantity
				}])
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				if(data == true){
					Swal.fire({
	    			  icon: 'success',
	    			  title: 'Check Out Successfull',
	    			  text: 'Happy Shopping!'
	    			  
	    			})

	    			navigate("/products");
				} else {
					Swal.fire({
	    			  icon: 'error',
	    			  title: 'Something went wrong',
	    			  text: 'Please try again'
	    			  
	    			})
				}
			})
		}

		function addProductToCart(){
			let productArray = [];
			if(localStorage.getItem('cart')){
				productArray = JSON.parse(localStorage.getItem('cart'))
			}

			productArray.push({'productId': productId,'quantity':quantity});
			localStorage.setItem('cart',JSON.stringify(productArray));
			Swal.fire({
	    			  icon: 'success',
	    			  title: 'Add to Cart Successfull',
	    			  text: 'Happy Shopping!'
	    			  
	    			})
			navigate('/products');

		}

		useEffect(()=>{
			console.log(typeof productId);
			fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
				.then(res => res.json())
				.then(data => {
					console.log(data);
					setName(data.name);
					setDescription(data.description);
					setPrice(data.price);
				})
		},[])

	return (

		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{ name }</Card.Title>
							{/*<Card.Subtitle>Description</Card.Subtitle>*/}
							<Card.Text>{ description }</Card.Text>
							<Card.Subtitle>Price</Card.Subtitle>
							<Card.Text>Php { price }</Card.Text>
							<InputGroup>
								<InputGroup.Text id="basic-addon1">quantity</InputGroup.Text>
								<Form.Control
									type="number"
									value={ quantity }
									min = "1"
									onChange={e => setQuantity(e.target.value)}

									required

								/>
								
							</InputGroup>
							{/*<Card.Subtitle>Class Schedule</Card.Subtitle>
							<Card.Text>8am-5pm</Card.Text>*/}
							{
								(user.id !== null && user.isAdmin == false) ?
									<Button variant="primary" onClick={()=> addProductToCart()} >AddToCart</Button>
								:

									<Link className="btn btn-danger btn-block" to="/login">Log in to CheckOut</Link>
							}
							
						</Card.Body>
					</Card>
					
				</Col>
			</Row>
			
		</Container>

	)
}