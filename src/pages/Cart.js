import {useEffect,useState} from 'react';
import CartCard from '../components/CartCard';
import {Button,Col,Row,Card} from 'react-bootstrap';
import {useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Cart(){

	const [product, setProduct] = useState([]);
	const [totalAmount, setTotalAmount] = useState(0);
	const [quantity,setQuantity] = useState([]);
	const navigate = useNavigate();

	const getTotal = ()=> {
		let preTotal =0;
		for(let x =0; x<product.length;x++){
			preTotal=(totalAmount + (quantity[x] * product[x].price) )
		}
		setTotalAmount(preTotal);
		//return preTotal
	}

	const checkOutProducts = () => {
			let j = 0;
			console.log(product);
			let checkOutArray = product.map(product=>{
				j=j+1;
				return {productId: product.key , quantity: quantity[j-1]}
			}) 

			console.log(checkOutArray);

			fetch(`${process.env.REACT_APP_API_URL}/users/checkOut`,{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					Authorization : `Bearer ${localStorage.getItem('token')}`
				},
				body : JSON.stringify(checkOutArray)
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				if(data == true){
					Swal.fire({
	    			  icon: 'success',
	    			  title: 'Check Out Successfull',
	    			  text: 'Happy Shopping!'
	    			  
	    			})

					localStorage.setItem('cart',JSON.stringify([]))
					setProduct([]);
					setQuantity([]);
	    			navigate("/products");
				} else {
					Swal.fire({
	    			  icon: 'error',
	    			  title: 'Something went wrong',
	    			  text: 'Please try again'
	    			  
	    			})
				}
			})
		}



	useEffect(()=>{

		//setQuantity(productArray.map(y => y.quantity));

		if(localStorage.getItem('cart') == null){
			null
		} else {
			let productArray = JSON.parse(localStorage.getItem('cart'));

			fetch(`${process.env.REACT_APP_API_URL}/products/multiple`,{
				method:'POST',
				headers: {
					'Content-type': 'application/json',
					Authorization : `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify(productArray)
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);
				let i = 0;
				let preTotal2 = 0
				//setQuantity(productArray.map(y => y.quantity));
				//console.log(quantity);
				setProduct(data.map(product =>{
					i=i+1;
					preTotal2 = preTotal2 + (product.price)*(productArray[i-1].quantity);
					return(
						<CartCard key={product._id} product ={product} quantity ={productArray[i-1].quantity} />
					)
				}))
				console.log(preTotal2)
				setTotalAmount(preTotal2);

				setQuantity(productArray.map(y => y.quantity));
			})
		}

	},[])

	//getTotal();

	return(

		<>
			<h2 className="text-center bg-dark text-light" >CART</h2>
			{product}
			<Card className="bg-dark text-light mt-2">
				<Row>
					
						
						<Col xs={9}>
							<div className="text-center">
								<Button variant="primary" onClick={()=> checkOutProducts()} className="w-100vw" >CheckOut</Button>
							</div>
						</Col>
						<Col xs={2} className="">
							<Card.Title className="tex">
								Total: PHP{totalAmount}
							</Card.Title>
							
						</Col>
				</Row>	
			</Card>
		</>
	)
}