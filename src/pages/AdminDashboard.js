import {useEffect,useState,useContext} from 'react';
import ProductCard from '../components/ProductCard'
import {Navigate,useNavigate} from 'react-router-dom';
import {Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';

import AddProduct from '../components/AddProduct';

//import coursesData from '../data/coursesData';

export default function AdminDashboard(){
	/*console.log(coursesData);
	console.log(coursesData[0]);*/

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	if(user.isAdmin == false){
		navigate("/products");
	}

	const [product, setProduct] = useState([]);

	const [showProductForm, setShowProductForm] = useState(false);

	const [changeTracker, setChangeTracker] = useState(false);

	console.log(`yaway=${changeTracker}`);

	const productForm = () => {
		setShowProductForm(true);
	}

	function deactivateProduct(productId){
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}

		})
		.then(res => res.json())
		.then(data => {

			if(data){
				Swal.fire({
	    			  icon: 'success',
	    			  title: 'Deactivation Successfull',
	    			  text: 'yey'
	    			  
	    			})
			} else {
				Swal.fire({
	    			  icon: 'error',
	    			  title: 'Deactivation failed',
	    			  text: 'Somethin went wrong'
	    			  
	    			})
			}
		})
	}

	console.log(`logchange${changeTracker}`);

	// Retrieves the products from the database upon initial render of the "products" component
	useEffect(()=> {

		fetch(`${process.env.REACT_APP_API_URL}/products/all`,{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
			.then(res => res.json())
			.then(data => {
				console.log(data);
				setProduct(data.map(product =>{
					return(
						<ProductCard key={product._id} product ={product} change={() => setChangeTracker(!changeTracker)} page="admin"  />
					)
				}))
			})
	},[changeTracker]);



	// const products =productsData.map(product =>{
	// 	return(
	// 		<productCard key={product.id} product ={product} />
	// 	)
	// })
	return(

		(user.isAdmin == false) ?
			<Navigate to="/products" />
		:

		<>
			{/*<productCard product={productsData[0]}/>*/}
			

			
			<h2 className="text-center p-3">Admin Dashboard</h2>
			<div className="text-center"> 
				{
					showProductForm ? 
						<AddProduct unShowAddProduct = {() => setShowProductForm(false)} change={() => setChangeTracker(!changeTracker)} />
					:
						<Button className="m-auto text-center" variant="primary" onClick={productForm}>Add product</Button>
				}
			</div>
			
			{/*<Button className="" variant="primary" onClick={productForm}>Add product</Button>*/}
			{product}
		</>


	)
}