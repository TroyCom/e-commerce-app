import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(){
	const {user, setUser } = useContext(UserContext);
	// State hooks to store the values of the input fields
	const [firstName ,setFirstName] = useState('');
	const [lastName ,setLastName] = useState('');
	//const [mobileNo ,setMobileNo] = useState('');

	const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    const navigate = useNavigate();

    // Function to user registration
    async function registerUser (e) {

    	// Prevents page redirection via form submission
    	e.preventDefault();

    	let isEmailValid =  await checkUserEmail(email)

    	console.log(isEmailValid)
    	//console.log(checkUserEmail(email));
    	if(isEmailValid == true) {

    		//console.log(isEmailValid)
    		Swal.fire({
    			  icon: 'error',
    			  title: 'Duplicate email found',
    			  text: 'Please provide a different email'
    			  
    		})
    	} else {
    		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
    			method: "POST",
    			headers: {
    				"Content-Type": "application/json"
    			},
    			body: JSON.stringify({
    				firstName : firstName,
    				lastName : lastName,
    				email: email,
    				password: password1
    				//mobileNo: mobileNo
    			})
    		})
    		.then(res => res.json())
    		.then(data => {
    			console.log(data);
    			Swal.fire({
    				  icon: 'success',
    				  title: 'Registration successful',
    				  text: 'Welcome to Troy Shop'
    				  
    			})

    			navigate("/login");

    		})
    	}
    	

    	
    }


    const checkUserEmail = async (email) => {
    	console.log(`hemail: ${email}`);

    	let response =	await (fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`,{
    		method: "POST",
    		headers: {
    			"Content-Type": "application/json",

    		},
    		body : JSON.stringify({
    			email: email
    		})
    	})
    	.then(res => res.json())
    	.then(data => {
    		console.log(data)

    		return data;
    	}))

    	console.log(response);
    	return response


    } 

    useEffect(() => {
    	// Validation to enable submit button when all fields are populated and both passwords match
    	if((email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
    		setIsActive(true);
    	} else {
    		setIsActive(false);
    	}
    }, [email, password1, password2]);

    return (

    	(user.id !== null) ?
    		<Navigate to="/courses" />

    	
    	:



    	<Form onSubmit={(e) => registerUser(e)} className="w-50 m-auto bg-dark text-light p-2">
            <h2>Register</h2>
    		<Form.Group controlId="userFirstName">
    			<Form.Label>First Name</Form.Label>
    			<Form.Control
    				type="text"
    				placeholder="Enter a first name"
    				value={ firstName }
    				onChange={e => setFirstName(e.target.value)}
    				required
    			 />
    			 
    		</Form.Group>

    		<Form.Group controlId="userLastName">
    			<Form.Label>Last Name</Form.Label>
    			<Form.Control
    				type="text"
    				placeholder="Enter a last name"
    				value={ lastName }
    				onChange={e => setLastName(e.target.value)}
    				required
    			 />
    			 
    		</Form.Group>



    		<Form.Group controlId="userEmail">
    			<Form.Label>Email Address</Form.Label>
    			<Form.Control
    				type="email"
    				placeholder="Enter an email"
    				value={ email }
    				onChange={e => setEmail(e.target.value)}
    				required
    			 />
    			 <Form.Text className="text-muted">We'll never share your email with anyone.</Form.Text>
    		</Form.Group>

    		{/*<Form.Group controlId="userMobileNo">
    			<Form.Label>Mobile Number</Form.Label>
    			<Form.Control
    				type="text"
    				placeholder="Enter a mobile number"
    				value={ mobileNo }
    				onChange={e => setMobileNo(e.target.value)}
    				required
    			 />
    			 
    		</Form.Group>*/}



    		<Form.Group controlId="password1">
    			<Form.Label>Password</Form.Label>
    			<Form.Control
    				type="password"
    				placeholder="Enter a password"
    				value={ password1 }
    				onChange={e => setPassword1(e.target.value)}
    				required
    			 />
    		</Form.Group>

    		<Form.Group controlId="password2">
    			<Form.Label>Confirm Password</Form.Label>
    			<Form.Control
    				type="password"
    				placeholder="Confirm your password"
    				value={ password2 }
    				onChange={e => setPassword2(e.target.value)}
    				required
    			 />
    		</Form.Group>
    		
    		{/*conditional rendering for submit button based on isActive state*/}
    		{
    			isActive ?
    				<Button variant="primary" type="submit" id="submitBtn" className="mt-2">Register</Button>
    				:
    				<Button variant="primary" type="submit" id="submitBtn" className="mt-2" disabled>Register</Button>
    		}
    	</Form>

    )
}