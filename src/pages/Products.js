import {useEffect,useState} from 'react';
import ProductCard from '../components/ProductCard'

//import coursesData from '../data/coursesData';

export default function Products(){
	/*console.log(coursesData);
	console.log(coursesData[0]);*/

	const [product, setProduct] = useState([]);

	// Retrieves the products from the database upon initial render of the "products" component
	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/products/`)
			.then(res => res.json())
			.then(data => {
				console.log(data);
				setProduct(data.map(product =>{
					return(
						<ProductCard key={product._id} product ={product} page="catalog"/>
					)
				}))
			})
	},[]);

	// const products =productsData.map(product =>{
	// 	return(
	// 		<productCard key={product.id} product ={product} />
	// 	)
	// })
	return(

		<>
			{/*<productCard product={productsData[0]}/>*/}
			<h2 className="text-center p-3">Our Products</h2>
			{product}
		</>


	)
}